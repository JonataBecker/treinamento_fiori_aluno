sap.ui.define([], function () {
	"use strict";
	return {
		getGenero: function (genero) {
			var resourceBundle = this.getView().getModel("i18n").getResourceBundle();
			switch (genero) {
				case "F":
					return resourceBundle.getText("generoFeminino");
				case "M":
					return resourceBundle.getText("generoMasculino");
				case "O":
					return resourceBundle.getText("generoOutro");
				default:
					return genero;
			}
		}
	};
});