/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"comtreinamento./zaluno_jdbecker/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});
