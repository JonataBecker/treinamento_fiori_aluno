sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"com/treinamento/zalunojdbecker/util/formatter"
],
	/**
	 * @param {typeof sap.ui.core.mvc.Controller} Controller
	 */
	function (Controller, Filter, FilterOperator, formatter, DateFormat) {
		"use strict";

		return Controller.extend("com.treinamento.zalunojdbecker.controller.View1", {

			_smartFilterBar: null,
			formatter: formatter
			onInit: function () {
				this._smartFilterBar = this.getView().byId("smartFilterBar");
			},
			onBeforeRebindTable: function (oEvent) {
				var mBindingParams = oEvent.getParameter("bindingParams");
				var oComboBox = this._smartFilterBar.getControlByKey("Genero");

				oComboBox.getSelectedKeys().forEach(key => {
					mBindingParams.filters.push(new Filter("Genero", FilterOperator.EQ, key));
				})

				const ativo = this._smartFilterBar.getControlByKey("Ativo").getState()
				mBindingParams.filters.push(new Filter("Ativo", FilterOperator.EQ, ativo));
			}
		});
	});
