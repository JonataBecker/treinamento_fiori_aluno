## Application Details
|               |
| ------------- |
|**Generation Date and Time**<br>Tue Oct 26 2021 20:04:32 GMT-0300 (Horário Padrão de Brasília)|
|**App Generator**<br>@sap/generator-fiori|
|**App Generator Version**<br>1.3.7|
|**Generation Platform**<br>Visual Studio Code|
|**Floorplan Used**<br>simple|
|**Service Type**<br>SAP System (ABAP On Premise)|
|**Service URL**<br>http://s4hana.stefanini.com.br:50000/sap/opu/odata/sap/ZALUNO_01_SRV
|**Module Name**<br>zaluno_jdbecker|
|**Application Title**<br>Alunos|
|**Namespace**<br>com.treinamento|
|**UI5 Theme**<br>sap_fiori_3|
|**UI5 Version**<br>Latest|
|**Enable Code Assist Libraries**<br>False|
|**Add Eslint configuration**<br>False|
|**Enable Telemetry**<br>True|

## zaluno_jdbecker

Cadastro de Alunos

### Starting the generated app

-   This app has been generated using the SAP Fiori tools - App Generator, as part of the SAP Fiori tools suite.  In order to launch the generated app, simply run the following from the generated app root folder:

```
    npm start
```

#### Pre-requisites:

1. Active NodeJS LTS (Long Term Support) version and associated supported NPM version.  (See https://nodejs.org)


